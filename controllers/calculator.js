var express = require('express');

exports.tambah = function(req, res) {
    var1 = parseInt(req.params.variabel1);
    var2 = parseInt(req.params.variabel2);
    result =var1+var2;
    res.status(200).json({
        success: true,
        result: result
    })
}

exports.kurang = function(req, res) {
    var1 = parseInt(req.params.variabel1);
    var2 = parseInt(req.params.variabel2);
    result = var1-var2;
    res.status(200).json({
        success: true,
        result: result
    })
}

exports.bagi = function(req, res) {
    var1 = parseInt(req.params.variabel1);
    var2 = parseInt(req.params.variabel2);
    result = var1/var2;
    res.status(200).json({
        success: true,
        result: result
    })
}

exports.kali = function(req, res) {
    var1 = parseInt(req.params.variabel1);
    var2 = parseInt(req.params.variabel2);
    result = var1*var2;
    res.status(200).json({
        success: true,
        result: result
    })
}