var express = require('express');
var router = express.Router();
const calculatorController = require('../controllers/calculator')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/tambah/:variabel1/:variabel2', calculatorController.tambah)
router.get('/kurang/:variabel1/:variabel2', calculatorController.kurang)
router.get('/bagi/:variabel1/:variabel2', calculatorController.bagi)
router.get('/kali/:variabel1/:variabel2', calculatorController.kali)

module.exports = router;
